use std::{
    ffi,
    fs,
    path::PathBuf,
    ptr,
};

use libc::c_char;

mod gjinstance;

#[no_mangle]
pub unsafe extern "C" fn libgj_init(
    wine_bin: *const c_char,
    pfx: *const c_char,
) -> *mut gjinstance::GJInstance {
    if wine_bin.is_null() || pfx.is_null() {
        return ptr::null_mut();
    }
    let wine_bin: PathBuf = match ffi::CStr::from_ptr(wine_bin).to_str() {
        Ok(wine_bin) => wine_bin.into(),
        _ => return ptr::null_mut(),
    };
    let pfx: PathBuf = match ffi::CStr::from_ptr(pfx).to_str() {
        Ok(pfx) => pfx.into(),
        _ => return ptr::null_mut(),
    };

    Box::into_raw(Box::new(gjinstance::GJInstance::new(wine_bin, pfx)))
}

#[no_mangle]
pub unsafe extern "C" fn libgj_rspawn(
    gji: *mut gjinstance::GJInstance,
    launch_data: *const c_char,
    log_path: *const c_char,
) -> gjinstance::GJError {
    if gji.is_null() || launch_data.is_null() {
        return gjinstance::GJError::GenericError;
    }

    let log = if !log_path.is_null() {
        ffi::CStr::from_ptr(log_path)
            .to_str()
            .ok()
            .and_then(|log_path| fs::File::create(log_path).ok())
    } else {
        None
    };

    let gji = &mut *gji;

    let launch_data: String = match ffi::CStr::from_ptr(launch_data).to_str() {
        Ok(launch_data) => launch_data.into(),
        _ => return gjinstance::GJError::GenericError,
    };

    let roblox = match gji.locate() {
        Some(roblox) => roblox,
        None => return gjinstance::GJError::RobloxNotFoundError,
    };

    gji.spawn(&roblox, &[&launch_data], log)
}

#[no_mangle]
pub unsafe extern "C" fn libgj_wait(gji: *mut gjinstance::GJInstance) -> gjinstance::GJError {
    if gji.is_null() {
        return gjinstance::GJError::GenericError;
    }
    let gji = &mut *gji;
    gji.wait()
}

#[no_mangle]
pub unsafe extern "C" fn libgj_is_installed(gji: *mut gjinstance::GJInstance) -> bool {
    if gji.is_null() {
        return false;
    }
    let gji = &mut *gji;
    gji.locate().is_some()
}

#[no_mangle]
pub unsafe extern "C" fn libgj_install(gji: *mut gjinstance::GJInstance) -> gjinstance::GJError {
    if gji.is_null() {
        return gjinstance::GJError::GenericError;
    }
    let gji = &mut *gji;
    gji.install(None)
}

#[no_mangle]
pub unsafe extern "C" fn libgj_destroy(gji: *mut gjinstance::GJInstance) {
    if !gji.is_null() {
        Box::from_raw(gji);
    }
}
