use std::{
    ffi::OsStr,
    fs,
    io::Write,
    os::unix::{
        io::{
            FromRawFd,
            IntoRawFd,
        },
        prelude::OsStrExt,
    },
    path::{
        Path,
        PathBuf,
    },
    process,
};

use walkdir::WalkDir;

///
#[repr(C)]
pub enum GJError {
    Success,
    WineError,
    FilesystemError,
    CurlError,
    RobloxNotFoundError,
    GenericError,
}

/// Represents a `Grapejuice` instance; holds the
/// directory holding `wine` and the `WINEPREFIX`
pub struct GJInstance {
    wine_bin: PathBuf,
    pfx: PathBuf,
}

impl GJInstance {
    pub fn new<A: Into<PathBuf>, B: Into<PathBuf>>(wine_bin: A, pfx: B) -> Self {
        Self {
            wine_bin: wine_bin.into(),
            pfx: pfx.into(),
        }
    }

    /// Create a wine instance, returns true on success and false on error
    pub fn spawn<P: AsRef<Path>, S: AsRef<OsStr>, I: IntoIterator<Item = S>>(
        &self,
        path: P,
        args: I,
        log: Option<fs::File>,
    ) -> GJError {
        // SAFETY: into_raw_fd is directly passed into from_raw_fd, no unsafety is
        // possible.
        let log = log
            .map(|log| unsafe { process::Stdio::from_raw_fd(log.into_raw_fd()) })
            .unwrap_or_else(process::Stdio::null);

        let mut child = match process::Command::new(&format!("{}/wine", self.wine_bin.display()))
            .arg(path.as_ref())
            .args(args)
            .env("WINEPREFIX", &self.pfx)
            .stderr(log)
            .spawn()
        {
            Ok(child) => child,
            Err(_) => return GJError::WineError,
        };
        child
            .wait()
            .map(|child| {
                if child.success() {
                    GJError::Success
                } else {
                    GJError::WineError
                }
            })
            .unwrap_or(GJError::WineError)
    }

    /// Wait for all wine processes in the wineprefix to exit via `wineserver`
    pub fn wait(&self) -> GJError {
        let mut child =
            match process::Command::new(&format!("{}/wineserver", self.wine_bin.display()))
                .arg("--wait")
                .env("WINEPREFIX", &self.pfx)
                .spawn()
            {
                Ok(child) => child,
                Err(_) => return GJError::WineError,
            };
        if child.wait().is_ok() { GJError::Success } else { GJError::WineError }
    }

    /// Locates RobloxPlayerLauncher.exe in the WINEPREFIX
    pub fn locate(&self) -> Option<PathBuf> {
        for entry in WalkDir::new(&self.pfx) {
            let entry = entry.ok()?;
            if entry
                .path()
                .file_name()
                .eq(&Some(OsStr::from_bytes(b"RobloxPlayerLauncher.exe")))
            {
                return Some(entry.path().to_owned());
            }
        }
        None
    }

    /// Downloads and installs Roblox into the WINEPREFIX
    pub fn install(&self, log: Option<fs::File>) -> GJError {
        const ROBLOX_PLAYER_LAUNCHER: &str = "/tmp/RobloxPlayerLauncher.exe";
        let mut f = match fs::File::create(ROBLOX_PLAYER_LAUNCHER) {
            Ok(f) => f,
            _ => return GJError::FilesystemError,
        };

        let mut easy = curl::easy::Easy::new();
        if easy.url("https://roblox.com/download/client").is_err()
            || easy.follow_location(true).is_err()
        {
            return GJError::CurlError;
        }

        let mut transfer = easy.transfer();
        if transfer
            .write_function(|data| {
                if f.write_all(data).is_err() {
                    Ok(0)
                } else {
                    Ok(data.len())
                }
            })
            .is_err()
            || transfer.perform().is_err()
        {
            return GJError::CurlError;
        }

        let status = self.spawn::<_, &'static str, _>(ROBLOX_PLAYER_LAUNCHER, [], log);
        _ = fs::remove_file(ROBLOX_PLAYER_LAUNCHER);
        status
    }
}
