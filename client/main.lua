-- luajit
local ffi = require("ffi")
ffi.cdef[[
void *libgj_init(const char *wine_bin, const char *pfx);
int libgj_rspawn(void *gji, const char *launch_data, const char *log_path);
int libgj_wait(void *gji);
bool libgj_is_installed(void *gji);
int libgj_install(void *gji);
void libgj_destroy(void *gji);
]]

if arg[1] == nil then
    print("usage: luajit main.lua <launch_data>")
    os.exit(1)
end
local wine_bin = "/usr/bin"
local pfx = os.getenv("HOME").."/.local/share/rblx"
local launch_data = arg[1]

local libgj = ffi.load("./libgj.so")
local gji = libgj.libgj_init(wine_bin, pfx)
if not libgj.libgj_is_installed(gji) then
    print("roblox not installed, attempting to install..")
    local err = libgj.libgj_install(gji)
    libgj.libgj_wait(gji)
    if err ~= 0 then
        print("roblox failed to install!: error code", err)
        os.exit(1)
    end
    print("roblox installed succesfully")
end
local err = libgj.libgj_rspawn(gji, launch_data, nil)
if err ~= 0 then
    print("libgj.libgj_rspawn failed!: error code", err)
    os.exit(1)
end
libgj.libgj_wait(gji)
libgj.libgj_destroy(gji)