import ctypes, os, sys
from enum import Enum

class libgj_GJError(Enum):
    libgj_Success = 0
    libgj_WineError = 1
    libgj_FilesystemError = 2
    libgj_CurlError = 3
    libgj_RobloxNotFoundError = 4
    libgj_GenericError = 5

libgj = ctypes.cdll.LoadLibrary(os.getcwd() + "/libgj.so")

# void *libgj_init(const char *wine_bin, const char *pfx);
libgj.libgj_init.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
libgj.libgj_init.restype = ctypes.c_void_p

# int libgj_rspawn(void *gji, const char *launch_data, const char *log_path);
libgj.libgj_rspawn.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
libgj.libgj_rspawn.restype = ctypes.c_int

# int libgj_wait(void *gji);
libgj.libgj_wait.argtypes = [ctypes.c_void_p]
libgj.libgj_wait.restype = ctypes.c_int

# bool libgj_is_installed(void *gji);
libgj.libgj_is_installed.argtypes = [ctypes.c_void_p]
libgj.libgj_is_installed.restype = ctypes.c_bool

# int libgj_install(void *gji);
libgj.libgj_install.argtypes = [ctypes.c_void_p]
libgj.libgj_install.restype = ctypes.c_int

# void libgj_destroy(void *gji);
libgj.libgj_destroy.argtypes = [ctypes.c_void_p]
libgj.libgj_destroy.restype = None


if len(sys.argv) <= 1:
    print(f"usage: {sys.argv[0]} <launch_data>")
    exit(1)

wine_bin = "/usr/bin"
pfx = os.path.expanduser("~") + "/.local/share/rblx"
launch_data = sys.argv[1]
gji = libgj.libgj_init(wine_bin.encode("utf-8"), pfx.encode("utf-8"))

if not gji:
    print("libgj_init failed!: null pointer", file = sys.stderr)
    exit(1)
elif not libgj.libgj_is_installed(gji):
    print("roblox isn't installed, attempting to install..")
    err = libgj_GJError(libgj.libgj_install(gji))
    libgj.libgj_wait(gji)
    if err == libgj_GJError.libgj_Success:
        print("roblox installation successful")
    else:
        print(f"libgj.libgj_install failed!: {err}", file = sys.stderr)
        exit(1)

err = libgj_GJError(libgj.libgj_rspawn(gji, launch_data.encode("utf-8"), None))
libgj.libgj_wait(gji)
if err != libgj_GJError.libgj_Success:
    print("libgj.libgj_rspawn failed!: {err}", file = sys.stderr)
libgj.libgj_destroy(gji)