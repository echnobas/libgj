#include <stdio.h>
#include <stdbool.h>

enum libgj_GJError {
    libgj_Success,
    libgj_WineError,
    libgj_FilesystemError,
    libgj_CurlError,
    libgj_RobloxNotFoundError,
    libgj_GenericError,
};

void *libgj_init(const char *wine_bin, const char *pfx);
int libgj_rspawn(void *gji, const char *launch_data, const char *log_path);
int libgj_wait(void *gji);
bool libgj_is_installed(void *gji);
int libgj_install(void *gji);
void libgj_destroy(void *gji);

int main() {
    enum libgj_GJError result;
    void *gji = libgj_init("/usr/bin", "/home/echnobas/.local/share/rblx");
    if (!gji) {
        fprintf(stderr, "libgj_init failed!: null_ptr\n");
    }
    if (libgj_is_installed(gji)) {
        result = libgj_rspawn(gji, "<LAUNCH_DATA_HERE>", NULL);
        if (result != libgj_Success) {
            fprintf(stderr, "libgj_rspawn failed!: error code %d\n", result);
            return 1;
        }
        result = libgj_wait(gji);
        if (result != libgj_Success) {
            fprintf(stderr, "libgj_wait failed!: error code %d\n", result);
            return 1;
        }
    } else {
        printf("Roblox is not installed, attempting to install\n");
        if ((result = libgj_install(gji)) != libgj_Success) {
            fprintf(stderr, "libgj_install failed!: error code %d\n", result);
            return 1;
        } else {
            printf("Roblox successfully installed\n");
        }
        libgj_wait(gji);
    }
    libgj_destroy(gji);
    return 0;
}